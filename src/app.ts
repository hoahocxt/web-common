import { ValidationPipe, Module } from '@nestjs/common';
import * as ClassValidatorModule from 'class-validator'

const CommonClassValidatorModule = {
  provide: 'CommonClassValidatorModule',
  useFactory: () => {
    return ClassValidatorModule;
  }
};

const CommonValidationPipe = {
  provide: 'CommonValidationPipe',
  useFactory: () => {
    return new ValidationPipe({
      disableErrorMessages: true,
    });
  }
};

const providers = [
  CommonClassValidatorModule,
  CommonValidationPipe
];

@Module({
  providers: [
    ...providers
  ],
  exports: [
    ...providers
  ]
})
export class SharedAppModule {

}