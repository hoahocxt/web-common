export enum RoleType {
  ADMIN = 'ADMIN',
  USER = 'USER',
  OPS = 'OPS',
}