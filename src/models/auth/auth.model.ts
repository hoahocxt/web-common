import { RoleType } from "../../constant";

export interface JwtPayload {
  userId: number;
  email: string;
  roles: RoleType[];
  isEnabledTFA: boolean;
}

export class JwtUserPayload implements JwtPayload {
  public userId: number;
  public email: string;
  public roles: RoleType[];
  public isEnabledTFA: boolean;
}