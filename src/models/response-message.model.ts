import { ApiModelProperty } from "@nestjs/swagger";
import { ResponseStatus } from "./response-status.model";
import { string } from 'joi';

export class ResponseMessage {
    @ApiModelProperty()
    public status: ResponseStatus;

    @ApiModelProperty()
    public messageCode: string;

    @ApiModelProperty()
    public message: string;

    @ApiModelProperty({
        isArray: true,
        type: string,
        default: []
    })
    public messages: string[];

    @ApiModelProperty()
    public data: any;

    public constructor(fields?: Partial<ResponseMessage>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}