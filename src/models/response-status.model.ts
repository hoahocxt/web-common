export enum ResponseStatus {
    Success = 1,
    Fail = 2,
    tfaVerify = 3,
    resendEmail = 4,
}