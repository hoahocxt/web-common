export class Role {
  public id: number;
  public name: string;

  constructor(fields?: Partial<Role>) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}