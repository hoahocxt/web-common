export class UserRole {
  public id: number;
  public userId: number;
  public roleId: number;

  constructor(fields?: Partial<UserRole>) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}