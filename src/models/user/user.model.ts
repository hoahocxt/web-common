import { ApiModelProperty } from '@nestjs/swagger';
import { IRequest } from "../interfaces/i-request";
import { IsNotEmpty, Matches, MaxLength, MinLength, IsNumber, IsDate } from 'class-validator';
import { EMAIL_REGEX, PASSWORD_REGEX } from "../../constant/common";
import { IResponse } from '../interfaces/i-response';
import { ResponseMessage } from '../response-message.model';
import { Role } from '../role';

export class User {
    @IsNotEmpty()
    @IsNumber()
    public id: number;

    @IsNotEmpty()
    @Matches(EMAIL_REGEX, {
        always: true
    })
    public email: string;

    @IsNotEmpty()
    @MinLength(10)
    public password: string;

    public name: string;

    public isEmailConfirm: boolean;

    public createTime: Date;

    @IsDate()
    public lastLoginTime: Date;

    public phoneNumber: string;

    public tfaSecret: string;

    public isEnabledTFA: boolean;

    public activationCode: string;

    public salt: string;

    public lastLoginFailTime: Date;

    public loginFailCount: number;

    public roles: Role[];

    constructor(fields?: Partial<User>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }

    public getRoles(): string[] {
        const authorities = [];
        for (const role of this.roles) {
            authorities.push(role.name);
        }
        return authorities;
    }
}

export class RegisterRequest implements IRequest {
    @IsNotEmpty()
    @Matches(EMAIL_REGEX, {
        always: true
    })
    @ApiModelProperty()
    public email: string;

    @IsNotEmpty()
    @MaxLength(50)
    @ApiModelProperty()
    public displayName: string;

    @IsNotEmpty()
    @MinLength(10)
    @Matches(PASSWORD_REGEX, {
        always: true
    })
    @ApiModelProperty()
    public password: string;

    @MaxLength(255)
    @ApiModelProperty()
    public referenceCode: string;

    constructor(fields?: Partial<RegisterRequest>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export class RegisterResponse implements IResponse {
    public responseMessage: ResponseMessage;

    public constructor(fields?: Partial<RegisterResponse>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export class ActivationCodePayload {
    public userId: number;
    public code: string;

    public created: number;

    constructor(fields?: Partial<ActivationCodePayload>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export class ActivateAccountRequest implements IRequest {
    @IsNotEmpty()
    @ApiModelProperty()
    activationCode: string;
    constructor(fields?: Partial<ActivateAccountRequest>) {
        if (fields) {
            Object.assign(this, fields);
        }
    };
}

export class ActivateAccountResponse implements IResponse {
    responseMessage: ResponseMessage;
    email: string;
    constructor(fields?: Partial<ActivateAccountResponse>) {
        if (fields) {
            Object.assign(this, fields);
        }
    };
}

export class ResendEmailRequest implements IRequest {
    @IsNotEmpty()
    @Matches(EMAIL_REGEX, {
        always: true
    })
    @ApiModelProperty()
    public email: string;

    @IsNotEmpty()
    @ApiModelProperty()
    public captcha: string;

    constructor(fields?: Partial<ResendEmailRequest>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export class ResendEmailResponse implements IResponse {
    public responseMessage: ResponseMessage;

    constructor(fields: Partial<ResendEmailResponse>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export class LoginRequest implements IRequest {
    @IsNotEmpty()
    @Matches(EMAIL_REGEX, {
        always: true
    })
    @ApiModelProperty()
    public email: string;

    @IsNotEmpty()
    @ApiModelProperty()
    public password: string;

    @ApiModelProperty()
    public capcha: string;

    @ApiModelProperty()
    public verifyCode?: string;

    constructor(fields?: Partial<LoginRequest>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}

export class LoginResponse implements IResponse {
    @ApiModelProperty()
    public responseMessage: ResponseMessage;

    @ApiModelProperty()
    public accessToken: string;

    @ApiModelProperty()
    public refreshAccessToken: string;

    @ApiModelProperty()
    public tfaToken: string;

    @ApiModelProperty()
    public socketToken: string;

    constructor(fields?: Partial<LoginResponse>) {
        if (fields) {
            Object.assign(this, fields);
        }
    }
}